﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Sdl.Desktop.IntegrationApi;
using Sdl.Desktop.IntegrationApi.Extensions;
using Sdl.TranslationStudioAutomation.IntegrationApi;
using Sdl.TranslationStudioAutomation.IntegrationApi.Extensions;
using Sdl.TranslationStudioAutomation.IntegrationApi.Presentation.DefaultLocations;
using System.Windows.Forms;

namespace Hello_World
{
    class MyCustomTradosStudio
    {
		[RibbonGroup("Sdl.HelloWorld", Name = "")]
		[RibbonGroupLayout(LocationByType = typeof(TranslationStudioDefaultRibbonTabs.HomeRibbonTabLocation))]
		class GCSButton : AbstractRibbonGroup
		{
		}

		[Action("Sdl.HelloWorld", Name = "GCS Plugin", Icon = "HelloWorldIcon", Description = "Hello World")]
		[ActionLayout(typeof(GCSButton), 20, DisplayType.Large)]
		class GCSButtonViewPartAction : AbstractAction
		{
			protected override void Execute()
			{
				MessageBox.Show("Hello World");
			}

		}
	}
}
