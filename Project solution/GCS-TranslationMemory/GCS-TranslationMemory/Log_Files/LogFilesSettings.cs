﻿using Sdl.Core.Settings;

namespace GCS_TranslationMemory.Log_Files
{
    public class LogFilesSettings : SettingsGroup
    {
        private readonly int _confirmationLevel = 0;
        public int ConfirmationLevelSetting
        {
            get { return GetSetting<int>(nameof(ConfirmationLevelSetting)); }
            set { GetSetting<int>(nameof(ConfirmationLevelSetting)).Value = value; }
        }

        public void ResetToDefaults()
        {
            ConfirmationLevelSetting = _confirmationLevel;
        }
        protected override object GetDefaultValue(string settingId)
        {
            switch (settingId)
            {
                case nameof(ConfirmationLevelSetting):
                    return _confirmationLevel;
            }
            return base.GetDefaultValue(settingId);
        }
    }
}
