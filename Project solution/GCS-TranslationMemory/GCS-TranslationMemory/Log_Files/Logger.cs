﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace GCS_TranslationMemory.Log_Files
{
    public static class Logger
    {
        public static List<LogEntity> logEntities = new List<LogEntity>();

        public static void generateLogger(string loggerFile)
        {
            try
            {
                if (!File.Exists(loggerFile))
                {
                    /* Check if the loggerFile for the specific SdlFile 
                     if it is not exist then create it*/
                    File.Create(loggerFile);

                    foreach (LogEntity entity in logEntities)
                    {
                        /* Using the StreamWriter to write each logEntitiy in the list
                         * down in the loggerFile */
                        using (StreamWriter outputFile = new StreamWriter(loggerFile))
                        {
                            outputFile.WriteLine(entity.LogDescription);
                        }
                    }
                }
                else
                {
                    /* If loggerFile already exist, read the text and append new line */
                    string currentContent = File.ReadAllText(loggerFile);
                    StringBuilder newContent = new StringBuilder();
                    foreach (LogEntity entity in logEntities)
                    {
                        newContent.AppendLine(entity.LogDescription);
                    }
                    // Write all the text last in first out
                    File.WriteAllText(loggerFile, newContent.ToString() + currentContent);
                }
            }

            catch (IOException ex)
            {
                throw new IOException(ex.ToString());
            }
        }
        public static List<string> ReadFile(string logFileName)
        {
            List<string> descriptions = new List<string>();
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                // read ref number file
                fs = new FileStream(logFileName, FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);
                string str = sr.ReadLine();
                while (str != null)
                {
                    descriptions.Add(str);
                    str = sr.ReadLine();
                }
                return descriptions;
            }
            catch (IOException)
            {
                return new List<string>();
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }
        }
    }
}
