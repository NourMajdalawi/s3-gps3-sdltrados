﻿using Sdl.ProjectAutomation.AutomaticTasks;
using Sdl.FileTypeSupport.Framework.IntegrationApi;
using Sdl.ProjectAutomation.Core;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GCS_TranslationMemory.Log_Files
{
    [AutomaticTask("SDL-TranslateMemoryPlugin-2",
                 "GCS-View logfiles",
                 "GCSView logfiles",
                 GeneratedFileType = AutomaticTaskFileType.BilingualTarget)]

    [AutomaticTaskSupportedFileType(AutomaticTaskFileType.BilingualTarget)]
    [RequiresSettings(typeof(LogFilesSettings), typeof(LogFilesSettingsPage))]
    class LogFiles : AbstractFileContentProcessingAutomaticTask
    {
        public string loggerFileName;
        private LogFilesSettings _settings;
        string path = @"C:\GCS-TranslationMemoryFiles\Loggers\";

        protected override void OnInitializeTask()
        {
            _settings = GetSetting<LogFilesSettings>();
        }

        protected override void ConfigureConverter(ProjectFile projectFile, IMultiFileConverter multiFileConverter)
        {
            this.loggerFileName = $"{path}/logger_{projectFile.Name}.txt";
        }

        public override void TaskComplete()
        {
            frmLogger frmLogger = new frmLogger(loggerFileName);
            frmLogger.ShowDialog();
        }
    }
}
