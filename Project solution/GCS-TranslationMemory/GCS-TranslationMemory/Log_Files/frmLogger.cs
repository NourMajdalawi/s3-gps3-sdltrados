﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GCS_TranslationMemory.Log_Files
{
    public partial class frmLogger : Form
    {
        public frmLogger(string logFileName)
        {
            InitializeComponent();
            lblTotalErrors.Text = Logger.ReadFile(logFileName).Count.ToString();
            foreach (var error in Logger.ReadFile(logFileName))
            {
                lbLogger.Items.Add(error + "\n");
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     
    }
}
