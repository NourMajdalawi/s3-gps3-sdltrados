﻿using Sdl.Desktop.IntegrationApi;
using System;
using GCS_TranslationMemory.Log_Files;
using System.Windows.Forms;

namespace GCS_TranslationMemory
{
    class LogFilesSettingsControl : UserControl, ISettingsAware<LogFilesSettings>
    {

        public LogFilesSettings Settings
        {
            get; set;

        }
        // Initializes the UI component
        public LogFilesSettingsControl()
        {
            InitializeComponent();
        }

        public void SetSettings(LogFilesSettings taskSettings)
        {
        }

        public void UpdateSettings(LogFilesSettings mySettings)
        {
            Settings = mySettings;
        }

        // Updates the UI elements to the corresponding settings
        public void UpdateUI(LogFilesSettings mySettings)
        {
            Settings = mySettings;
            this.UpdateSettings(Settings);
        }
        // The control elements on the UI are configured with the corresponding values
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.SetSettings(Settings);
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LogFilesSettingsControl
            // 
            this.Name = "LogFilesSettingsControl";
            this.Size = new System.Drawing.Size(273, 124);
            this.ResumeLayout(false);

        }
    }
}
