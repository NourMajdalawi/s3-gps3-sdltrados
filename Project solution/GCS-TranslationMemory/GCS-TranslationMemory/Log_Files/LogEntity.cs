﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCS_TranslationMemory.Log_Files
{
    public class LogEntity
    {
        private string referenceNumber;
        private string errorDescription;
        private string logDescription;

        //Class created to assign each reference number to its error
        public LogEntity(string referenceNumber, string errorDescription)
        {
            this.referenceNumber = referenceNumber;
            this.ErrorDescription = errorDescription;
        }

        public string ReferenceNumber
        {
            get
            {
                return this.referenceNumber;
            }
        }

        public string ErrorDescription
        {
            get
            {
                return this.errorDescription;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    this.errorDescription = value;
                    // In case any error occur set directly the logDescription with the same value
                    this.logDescription = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " - REF-Nr.:" + this.referenceNumber + " - " + value;
                }
            }

        }

        public string LogDescription
        {
            get { return this.logDescription; }
        }

        public override string ToString()
        {
            return this.ReferenceNumber + ": " + this.ErrorDescription;
        }
    }
}
