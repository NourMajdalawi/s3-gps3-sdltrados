﻿using Sdl.FileTypeSupport.Framework.BilingualApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using GCS_TranslationMemory.Log_Files;

namespace GCS_TranslationMemory
{
    class FileReader : AbstractBilingualContentProcessor
    {

        private readonly CreateTranslationMemorySettings _taskSettings;
        private readonly string _inputFilePath;
        private StreamWriter _outFile;

        private HashSet<string> matches;
        private string sourceLanguage;
        private string targetLanguage;
        private string loggerFileName;
        private string completeSourceLanguage;

        public FileReader(CreateTranslationMemorySettings settings, string filePath, string sourceLanguage, string targetLanguage, string completeSourceLanguage, string loggerFileName)
        {
            this._taskSettings = settings;
            this._inputFilePath = filePath;
            this.sourceLanguage = sourceLanguage;
            this.targetLanguage = targetLanguage;
            this.loggerFileName = loggerFileName;
            this.completeSourceLanguage = completeSourceLanguage;
            this.matches = new HashSet<string>();
        }

        #region SDL interface methods
        public override void SetFileProperties(IFileProperties fileInfo)
        {
            _outFile = new StreamWriter(_inputFilePath + ".txt");
        }
        public override void ProcessParagraphUnit(IParagraphUnit paragraphUnit)
        {

            if (paragraphUnit.IsStructure)
            {
                return;
            }

            foreach (ISegmentPair item in paragraphUnit.SegmentPairs)
            {
                // Pattern that mainly used to get the deseired reference numbers
                string pattern = @"\b(\b((?<= )|(?<=\s))([1-9][0-9]?[0-9]?[0-9]?)( *)\/( *)(19[5-9][0-9]|20[0-2][0-9]|2030)(( *)\/( *)[A-Z]{2})?|((19[5-9][0-9]|20[0-2][0-9]|2030)( *)\/( *)([1-9][0-9]?[0-9]?[0-9]?)(( *)\/( *)[A-Z]{2})?))\b";
                Regex rg = new Regex(pattern);

                // get all the strings of the source paragraph

                string[] words = item.Source.ToString().Split(' ');
                for (int j = 0; j < words.Length; j++)
                {
                    // If any matched reference number found, add it to the matchCollection

                    MatchCollection matchedReferences = rg.Matches(words[j]);
                    for (int i = 0; i < matchedReferences.Count; i++)
                    {
                        // In case the reference number has typos, correct them

                        string reference = matchedReferences[i].Value.Replace(" ", "");

                        /* HashSet used cause it will only add unique values 
                         * When it's Add true it means it is unique*/
                        if (matches.Add(reference))
                        {
                            try
                            {
                                //Align the txm file
                                DocumentHandler.alignTMX(reference, sourceLanguage, targetLanguage, completeSourceLanguage, this.loggerFileName);
                            }
                            catch (Exception ex)
                            {
                                /*In case any error occurs in alignning process, add new occured
                                 * error to the static logEntities list */

                                Logger.logEntities.Add(new LogEntity(reference, ex.Message));
                            }

                        }
                    }
                }
            }
        }

        public override void FileComplete()
        {
            base.FileComplete();
            _outFile.Close();
        }

        public override void Complete()
        {
            base.Complete();
        }
        #endregion


        public List<string> getReferenceNumberList()
        {
            return matches.ToList();
        }
        public string getReferenceNumbers()
        {
            /* get the matches HashSet as string in order to use it to display
             the reference numbers in the dialog box*/

            List<string> entities = new List<string>();
            foreach (string refNumber in matches)
            {
                /* Check if the logEntities list has items with the same reference number =>
                 if they are equal that means that this reference number has a problem to show
                  add the found logEntity to the generated entities list */

                LogEntity logEntity = Logger.logEntities.Find(item => item.ReferenceNumber == refNumber);
                if (logEntity != null)
                {
                    entities.Add(logEntity.ToString());
                }
                else
                {
                    /* if the reference number is not found in the logEntities list
                     then add the reference number to the entities list*/
                    entities.Add(refNumber);
                }
            }
            return ListAsString(entities);
        }

        private string ListAsString(List<string> entities)
        {
            string str = "";
            foreach (var item in entities)
            {
                str += item + "\n";
            }
            return str;
        }

    }
}
