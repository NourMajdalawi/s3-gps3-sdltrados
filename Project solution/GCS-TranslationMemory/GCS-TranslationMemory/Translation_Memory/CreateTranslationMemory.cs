﻿using Sdl.ProjectAutomation.AutomaticTasks;
using Sdl.FileTypeSupport.Framework.IntegrationApi;
using Sdl.ProjectAutomation.Core;
using System.Collections.Generic;
using GCS_TranslationMemory.Log_Files;
using System.Windows.Forms;
using System.IO;
using System;

namespace GCS_TranslationMemory
{
    [AutomaticTask("SDL-TranslateMemoryPlugin-1",
                   "Create Translate Memory",
                   "SDL-TranslateMemoryPlugin",
                   //[TODO] You can change the file type according to your needs
                   GeneratedFileType = AutomaticTaskFileType.BilingualTarget)]
    //[TODO] You can change the file type according to your needs
    [AutomaticTaskSupportedFileType(AutomaticTaskFileType.BilingualTarget)]
    [RequiresSettings(typeof(CreateTranslationMemorySettings), typeof(CreateTranslationMemorySettingsPage))]
    class CreateTranslationMemory : AbstractFileContentProcessingAutomaticTask
    {
        static string path = @"C:\GCS-TranslationMemoryFiles\Loggers\";
        private CreateTranslationMemorySettings _settings;
        FileReader _task;
        string targetLanguage;
        string sourceLanguage;
        string loggerFile;

        protected override void OnInitializeTask()
        {
            _settings = GetSetting<CreateTranslationMemorySettings>();

            /*check if the specified directory where the Logges should be place exist
             in case it is not, then create the directory*/

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        protected override void ConfigureConverter(ProjectFile projectFile, IMultiFileConverter multiFileConverter)
        {
            /* For each batch task there would be one loggerFile
             Extact the loggerFile name from the ProjectFile*/

            this.loggerFile = $"{path}/logger_{projectFile.Name}.txt";

            // retrieve source and target language from the projectFile
            this.targetLanguage = projectFile.GetLanguageDirection().TargetLanguage.ToString();
            this.sourceLanguage = projectFile.SourceFile.Language.ToString();

            /* targetLanguage is e.g. en-EN
               sourceLanguage is nl-NL
            split them by (-) and give the first index string */
            string target = this.targetLanguage.Split('-')[0];
            string source = this.sourceLanguage.Split('-')[0];

            /*Generate the FileReader passing the complete sourceLanguage
             * in order to create the Header for a TMX file */

            _task = new FileReader(_settings, projectFile.LocalFilePath, source, target, sourceLanguage, loggerFile);

            multiFileConverter.AddBilingualProcessor(_task);
            multiFileConverter.Parse();
        }

        public override void TaskComplete()
        {
            List<string> refNumbers = _task.getReferenceNumberList();

            try
            {
                // try to save the TMX file and delete the pre-downloaded Html files 
                DocumentHandler.Save();
            }
            catch (Exception ex)
            {
                /*  saving error is related to the reference numbers 
                 *  that didn't have any other errors
                 * and successfully downloaded via Eur-Lex webservice.*/

                foreach (string refNumber in refNumbers)
                {
                    /* check if logEntities list has one of the found reference numbers
                     * if it does that mean that there is an error occur in it
                     * if not, we will add the error to the the list
                     */
                    LogEntity logEntity = Logger.logEntities.Find(item => item.ReferenceNumber == refNumber);
                    if (logEntity == null)
                    {
                        Logger.logEntities.Add(new LogEntity(refNumber, ex.Message));
                    }
                }
            }
            finally
            {
               /* If the list of erros has entities, generate the logger file
                for the specific batch task file */
                if (Logger.logEntities.Count > 0)
                {
                    Logger.generateLogger(this.loggerFile);
                }
                DialogResult d;
                d = MessageBox.Show(_task.getReferenceNumbers(), "Reference Numbers", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }
    }
}
