﻿using System.Net;
using System.IO;
using System.Xml;
using System;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using Sdl.LanguagePlatform.Core.Tokenization;
using Sdl.LanguagePlatform.TranslationMemory;
using Sdl.LanguagePlatform.TranslationMemoryApi;
using GCS_TranslationMemory.Log_Files;

namespace GCS_TranslationMemory
{
    public class DocumentHandler
    {
        static string celex;
        static string xmlPath;
        static List<string> htlmPaths = new List<string>();
        public static XmlDocument doc;
        private static XmlElement body;

        private static string loggerFile;
        private static string source_language;
        private static string target_language;
        static string path = @"C:\GCS-TranslationMemoryFiles\HTML-Documents\";

        public static void getCelex(string year, string reference_number, string source_language)
        {
            //create the expert query
            string expertquery = "DTA = " + year + " AND DTN = " + reference_number;
            //call the webservice
            celex = EUWebService.returnCelexNumber(expertquery, source_language);
        }

        public static void downloadHTMLDocuments(string year, string reference_number, string source_language, string target_language)
        {
            using (WebClient client = new WebClient()) // WebClient class inherits IDisposable
            {

                string htmlSourcePath = "";
                string htmlTargetPath = "";
                getCelex(year, reference_number, source_language);
                if (!File.Exists($"{path}CELEX_" + celex + "_" + source_language + ".html") && !File.Exists($"{path}CELEX_" + celex + "_" + target_language + ".html"))
                {
                    htmlSourcePath = $"{path}CELEX_" + celex + "_" + source_language + ".html";
                    htmlTargetPath = $"{path}CELEX_" + celex + "_" + target_language + ".html";

                    // download
                    try
                    {
                        client.DownloadFile("https://eur-lex.europa.eu/legal-content/" + source_language + "/TXT/HTML/?uri=CELEX:" + celex, htmlSourcePath);
                        client.DownloadFile("https://eur-lex.europa.eu/legal-content/" + target_language + "/TXT/HTML/?uri=CELEX:" + celex, htmlTargetPath);
                    }

                    catch (WebException ex)
                    {
                        throw new Exception("The source or target or both texts are not available on EUR-Lex for the corresponding reference number");
                    }
                    catch (IOException ex)
                    {
                        throw new Exception("There is not enough space to store all the HTML documents into a translation memory");
                    }

                    // Save source and target HTML path in order to delete them later.
                    htlmPaths.Add(htmlSourcePath);
                    htlmPaths.Add(htmlTargetPath);
                }
            }
        }

        public static void generateTmxFile(string sourceLanguage, string targetLanguage, string loggerFile, string completeSourceLanguage)
        {
            // Main XML document.
            doc = new XmlDocument();
            doc.PreserveWhitespace = false;
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            // every xml file has single root.
            XmlElement tmx = doc.CreateElement(string.Empty, "tmx", string.Empty);
            tmx.SetAttribute("version", "1.4");
            doc.AppendChild(tmx);

            //getting the computer name and the user.
            string computerName = Environment.MachineName.ToString();
            string user = Environment.UserName.ToString();

            //every xml file has a single header.
            XmlElement header = doc.CreateElement(string.Empty, "header", string.Empty);
            header.SetAttribute("creationtool", "SDL Language Platform");
            header.SetAttribute("creationtoolversion", "8.1");
            header.SetAttribute("o-tmf", "TW4Win 2.0 Format");
            header.SetAttribute("o-datatype", "rtf");
            header.SetAttribute("segtype", "sentence");
            header.SetAttribute("adminlang", completeSourceLanguage.ToUpper());
            header.SetAttribute("srclang", completeSourceLanguage.ToUpper());
            header.SetAttribute("creationdate", DateTime.UtcNow.ToString(("yyyyMMddTHHmmssZ")));
            header.SetAttribute("creationid", $"{computerName}/{user}");
            tmx.AppendChild(header);

            // body is root.
            body = doc.CreateElement(string.Empty, "body", string.Empty);
            tmx.AppendChild(body);

            source_language = sourceLanguage;
            target_language = targetLanguage;
            DocumentHandler.loggerFile = loggerFile;
        }

        private static bool parseValue(string[] values, ref string year, ref string referenceNumber)
        {
            // year has 4 chars

            // ref numbers with country code are skipped
            if (values.Length == 3)
            {
                return false;
            }
            if (values[0].Length == 4)
            {
                int parsedValue = int.Parse(values[0]);

                if (parsedValue >= 1950 && parsedValue <= 2030)
                {
                    year = values[0];
                    referenceNumber = values[1];
                }
                else
                {
                    year = values[1];
                    referenceNumber = values[0];
                }
            }
            else
            {
                referenceNumber = values[0];
                year = values[1];
            }

            return true;
        }

        /// <summary>
        /// Read two HTML files and create a tmx out of them
        /// </summary>
        public static void alignTMX(string reference, string source_language, string target_language, string completeSourceLanguage, string loggerFileName)
        {
            // Check if there is no directory with specified path, create it
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // If there is no document yet to align the TXM on, generate a new one
            if (doc == null)
            {
                generateTmxFile(source_language, target_language, loggerFileName, completeSourceLanguage);
            }

            string[] values = reference.Split('/');
            string year = "";
            string referenceNumber = "";

            // ref numbers with country code are skipped
            if (!parseValue(values, ref year, ref referenceNumber))
            {
                return;
            }

            // download both source and target html documents
            downloadHTMLDocuments(year, referenceNumber, source_language, target_language);

            try
            {
                //source language is: the french - dutch - polish
                HtmlDocument document_source = new HtmlDocument();
                string sourceData = File.ReadAllText($"{path}CELEX_" + celex + "_" + source_language + ".html");
                document_source.LoadHtml(sourceData);

                //target language is english
                HtmlDocument document_target = new HtmlDocument();
                string targetData = File.ReadAllText($"{path}CELEX_" + celex + "_" + target_language + ".html");
                document_target.LoadHtml(targetData);

                // extract all p tags
                HtmlNodeCollection source_collection = document_source.DocumentNode.SelectNodes("//p");
                HtmlNodeCollection target_collection = document_target.DocumentNode.SelectNodes("//p");

                for (int i = 0; i < source_collection.Count; i++)
                {
                    // tu
                    XmlElement tu = doc.CreateElement(string.Empty, "tu", string.Empty);
                    tu.SetAttribute("creationdate", DateTime.UtcNow.ToString("yyyyMMddTHHmmssZ"));
                    tu.SetAttribute("creationid", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                    tu.SetAttribute("changedate", DateTime.UtcNow.ToString("yyyyMMddTHHmmssZ"));
                    tu.SetAttribute("changeid", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                    tu.SetAttribute("lastusagedate", DateTime.UtcNow.ToString("yyyyMMddTHHmmssZ"));
                    body.AppendChild(tu);

                    // properties
                    XmlElement prop_source = doc.CreateElement("prop");
                    prop_source.SetAttribute("type", "Txt::SourceFile");
                    XmlText prop_source_Text = doc.CreateTextNode($"CELEX_{celex}_{source_language}_TXT_CELEX_{celex}_{target_language}_TXT.sdlalign");
                    prop_source.AppendChild(prop_source_Text);
                    tu.AppendChild(prop_source);

                    XmlElement prop_target = doc.CreateElement("prop");
                    prop_target.SetAttribute("type", "Txt::TargetFile");
                    XmlText prop_target_Text = doc.CreateTextNode($"CELEX_{celex}_{source_language}_TXT_CELEX_{celex}_{target_language}_TXT.sdlalign");
                    prop_target.AppendChild(prop_target_Text);
                    tu.AppendChild(prop_target);

                    XmlElement prop_quality = doc.CreateElement("prop");
                    prop_quality.SetAttribute("type", "Txt::Quality");
                    XmlText prop_quality_Text = doc.CreateTextNode("100");
                    prop_quality.AppendChild(prop_quality_Text);
                    tu.AppendChild(prop_quality);

                    // source lang tuv
                    XmlElement source_tuv = doc.CreateElement("tuv");
                    source_tuv.SetAttribute("xml:lang", $"{source_language.ToUpper()}");

                    XmlElement source_seg = doc.CreateElement(string.Empty, "seg", string.Empty);
                    XmlText source_seg_text = doc.CreateTextNode(source_collection[i].InnerText.Trim());

                    source_seg.AppendChild(source_seg_text);
                    source_tuv.AppendChild(source_seg);
                    tu.AppendChild(source_tuv);

                    // target lang tuv
                    XmlElement target_tuv = doc.CreateElement(string.Empty, "tuv", string.Empty);
                    target_tuv.SetAttribute("xml:lang", $"{target_language.ToUpper()}");

                    XmlElement target_seg = doc.CreateElement(string.Empty, "seg", string.Empty);
                    XmlText target_seg_text = doc.CreateTextNode(target_collection[i].InnerText.Trim());

                    target_seg.AppendChild(target_seg_text);
                    target_tuv.AppendChild(target_seg);
                    tu.AppendChild(target_tuv);
                }
            }
            catch (Exception)
            {
                throw new Exception("The text aligning process failed");
            }
        }
        public static void Save()
        {
            try
            {
                // save with .tmx extension
                xmlPath = $"{path}CELEX_" + celex + "_" + source_language + "_" + target_language + ".tmx";

                // delete previously dowloaded HTML file that are no more needed
                deleteHTMLFiles();
                doc.Save(xmlPath);

                /* make the doc and body null in order to make another XMl file
                 * for the other SDL file
                */
                doc = null;
                body = null;
            }
            catch (XmlException)
            {
                throw new Exception("Failed to convert the aligned HTML documents to a TMX format");
            }
        }

        public static void deleteHTMLFiles()
        {
            /*after generating the TXM delete the HTML files that used 
             * for this process and no longer needed
            */
            foreach (string item in htlmPaths)
            {
                File.Delete(item);
            }
        }


    }
}

