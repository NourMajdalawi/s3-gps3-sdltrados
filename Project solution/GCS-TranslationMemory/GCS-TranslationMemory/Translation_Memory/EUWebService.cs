﻿using System;
using System.IO;
using System.Net;
using System.Xml;

namespace GCS_TranslationMemory
{
    public class EUWebService
    {
        public static string returnCelexNumber(string query, string language)
        {
            // call EUR-LEX web service
            string searchURL = "http://eur-lex.europa.eu/search";
            string result = CallWebService(query, language, true, searchURL);

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(result);
            if (xmlDocument.GetElementsByTagName("ID_CELEX").Item(0) == null)
            {
                result = CallWebService(query, language, false, searchURL);
                xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(result);
                if (xmlDocument.GetElementsByTagName("ID_CELEX").Item(0) != null)
                {
                    throw new Exception("The found reference document is no longer in force");
                }
                else
                {
                    throw new Exception("​The found reference number does not exist in the EUR-Lex database");
                }

            }
            string celex = xmlDocument.GetElementsByTagName("ID_CELEX").Item(0).InnerText;
            return celex;
        }

        /// <summary>
        /// This method returns CELEX ID based on expert query and language 
        /// </summary>
        /// <param name="expertquery"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        public static string CallWebService(string query, string language, bool inForce, string searchURL)
        {
            try
            {
                var _url = "https://eur-lex.europa.eu/EURLexWebService";
                XmlDocument soapEnvelopXml = CreateSoapEnvelope(query, language, inForce, searchURL);
                HttpWebRequest webRequest = CreateWebRequest(_url);
                InsertSoapEnvelopIntoWebRequest(soapEnvelopXml, webRequest);

                IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);
                asyncResult.AsyncWaitHandle.WaitOne();


                string soapResult;
                using (WebResponse web = webRequest.EndGetResponse(asyncResult))
                {
                    using (StreamReader sr = new StreamReader(web.GetResponseStream()))
                    {
                        soapResult = sr.ReadToEnd();
                    }
                    return soapResult;
                }
            }
            catch (WebException)
            {
                throw new Exception(" ​Connection failed to EUR-Lex database");
            }
        }

        private static void InsertSoapEnvelopIntoWebRequest(XmlDocument soapEnvelopXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopXml.Save(stream);
            }
        }

        private static HttpWebRequest CreateWebRequest(string url)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/soap+xml;charset=\"UTF-8\"";
            httpWebRequest.MediaType = "application/soap+xml;charset=\"UTF-8\"";
            httpWebRequest.Accept = "application/soap+xml;charset=\"UTF-8\"";
            httpWebRequest.Method = "POST";
            return httpWebRequest;

        }

        public static XmlDocument CreateSoapEnvelope(string query, string language, bool inForce, string searchURL)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(
                $@"<soap:Envelope xmlns:sear=""{searchURL}"" xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"">
               <soap:Header>
                  <wsse:Security soap:mustUnderstand = ""true"" xmlns:wsse = ""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu = ""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
                     <wsse:UsernameToken wsu:Id = ""UsernameToken-EA593C63799B3DDB7816051059387392"">
                        <wsse:Username>n0055fa7</wsse:Username>
                        <wsse:Password Type =""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"">RM8yyZtlqqw</wsse:Password>
                     </wsse:UsernameToken>
                  </wsse:Security>
               </soap:Header>
               <soap:Body>
                  <sear:searchRequest>
                     <sear:expertQuery> {query} AND VV = {inForce}</sear:expertQuery>
                     <sear:page> 1 </sear:page>
                     <sear:pageSize> 10 </sear:pageSize>
                     <sear:searchLanguage> {language} </sear:searchLanguage>
                  </sear:searchRequest>
               </soap:Body>
            </soap:Envelope>"
                );
            return xmlDocument;
        }
    }
}
