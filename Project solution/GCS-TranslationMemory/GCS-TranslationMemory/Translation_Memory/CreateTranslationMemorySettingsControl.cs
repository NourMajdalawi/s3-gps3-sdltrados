﻿using Sdl.Desktop.IntegrationApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GCS_TranslationMemory
{
    class CreateTranslationMemorySettingsControl : UserControl, ISettingsAware<CreateTranslationMemorySettings>
    {
        private GroupBox groupBox1;
        private ComboBox combo_status;

        public CreateTranslationMemorySettings Settings
        {
            get; set;

        }
        // Initializes the UI component
        public CreateTranslationMemorySettingsControl()
        {
            InitializeComponent();
        }

        public void SetSettings(CreateTranslationMemorySettings taskSettings)
        {
            // sets the UI element, i.e. the status dropdown list to the corresponding segment status value
            Settings = taskSettings;
            SettingsBinder.DataBindSetting<int>(combo_status, "SelectedIndex", Settings, nameof(Settings.ConfirmationLevelSetting));
            UpdateUI(taskSettings);
        }

        public void UpdateSettings(CreateTranslationMemorySettings mySettings)
        {
            Settings = mySettings;
        }

        // Updates the UI elements to the corresponding settings
        public void UpdateUI(CreateTranslationMemorySettings mySettings)
        {
            Settings = mySettings;
            this.UpdateSettings(Settings);
        }
        // The control elements on the UI are configured with the corresponding values
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.SetSettings(Settings);
        }
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.combo_status = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.combo_status);
            this.groupBox1.Location = new System.Drawing.Point(13, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 81);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // combo_status
            // 
            this.combo_status.FormattingEnabled = true;
            this.combo_status.Items.AddRange(new object[] {
            "Find Refrences"});
            this.combo_status.Location = new System.Drawing.Point(22, 31);
            this.combo_status.Name = "combo_status";
            this.combo_status.Size = new System.Drawing.Size(186, 24);
            this.combo_status.TabIndex = 0;
            // 
            // MyCustomBatchTaskSettingsControl
            // 
            this.Controls.Add(this.groupBox1);
            this.Name = "MyCustomBatchTaskSettingsControl";
            this.Size = new System.Drawing.Size(273, 124);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
    }
}
