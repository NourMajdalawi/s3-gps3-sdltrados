﻿using Sdl.FileTypeSupport.Framework.IntegrationApi;
using Sdl.ProjectAutomation.AutomaticTasks;
using Sdl.ProjectAutomation.Core;

namespace GCS_TranslationMemory.Feedback
{
    [AutomaticTask("Feedback-1",
               "Feedback",
               "Feedback",
               GeneratedFileType = AutomaticTaskFileType.BilingualTarget)]

    [AutomaticTaskSupportedFileType(AutomaticTaskFileType.BilingualTarget)]
    [RequiresSettings(typeof(FeedbackSettings), typeof(FeedbackSettingsPage))]
    public class Feedback : AbstractFileContentProcessingAutomaticTask
    {
        private FeedbackSettings _settings;

        protected override void OnInitializeTask()
        {
            _settings = GetSetting<FeedbackSettings>();
        }

        protected override void ConfigureConverter(ProjectFile projectFile, IMultiFileConverter multiFileConverter)
        {
        }

        public override void TaskComplete()
        {
            frmFeedback frmFeedback = new frmFeedback();
            frmFeedback.ShowDialog();
        }
    }
}
