﻿using Sdl.Desktop.IntegrationApi;
using System;
using GCS_TranslationMemory.Log_Files;
using System.Windows.Forms;

namespace GCS_TranslationMemory.Feedback
{
    class FeedbackSettingsControl : UserControl, ISettingsAware<FeedbackSettings>
    {

        public FeedbackSettings Settings
        {
            get; set;

        }
        // Initializes the UI component
        public FeedbackSettingsControl()
        {
            InitializeComponent();
        }

        public void SetSettings(FeedbackSettings taskSettings)
        {
        }

        public void UpdateSettings(FeedbackSettings mySettings)
        {
            Settings = mySettings;
        }

        // Updates the UI elements to the corresponding settings
        public void UpdateUI(FeedbackSettings mySettings)
        {
            Settings = mySettings;
            this.UpdateSettings(Settings);
        }
        // The control elements on the UI are configured with the corresponding values
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.SetSettings(Settings);
        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LogFilesSettingsControl
            // 
            this.Name = "FeedbackSettingsControl";
            this.Size = new System.Drawing.Size(273, 124);
            this.ResumeLayout(false);

        }
    }
}
