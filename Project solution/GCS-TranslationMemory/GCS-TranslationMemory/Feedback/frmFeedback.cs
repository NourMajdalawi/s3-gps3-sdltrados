﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GCS_TranslationMemory.Feedback
{
    public partial class frmFeedback : Form
    {
        string imageName = string.Empty;
        string imageLabel;
        public frmFeedback()
        {
            InitializeComponent();
            dtpDate.CustomFormat = "dd/MM/yyyy HH:mm";
        }
        private void LoadImage()
        {
            try
            {
                Thread thread = new Thread((ThreadStart)(() =>
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        imageName = ofd.FileName;
                        imageLabel = ofd.SafeFileName;
                    }
                }));
                var t = thread;
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
            }
            catch (IOException)
            {
                MessageBox.Show("An error occured while attempting to open your file!");
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            LoadImage();
            this.lblImageName.Text = imageLabel;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnCreate_Click(object sender, EventArgs e)
        {
            this.SendEmail();
        }
        public void SendEmail()
        {
            string title = tbTitle.Text;
            string description = tbDescription.Text;
            string urgency;
            string receiverEmail;
            DateTime date;
            try
            {
                date = dtpDate.Value;
                receiverEmail = "nourmagdalawi1@gmail.com";
                //Get the users email and mails them their credentials
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;

                MailMessage message = new MailMessage();
                message.From = new MailAddress("ofortunaamplexor@gmail.com");
                message.To.Add(receiverEmail);
                if (rbLow.Checked)
                {
                    urgency = rbLow.Text;
                }
                else if (rbMedium.Checked)
                {
                    urgency = rbMedium.Text;
                }
                else
                {
                    urgency = rbHigh.Text;
                }
                title = $"Urgency: {urgency.ToUpper()} - { title}";
                message.Subject = title;
                message.Body = $"{date.ToString()} <br> {description}";
                if (this.imageName != string.Empty)
                {
                    message.Attachments.Add(new Attachment(this.imageName));
                }
                client.UseDefaultCredentials = false;
                message.IsBodyHtml = true;
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("ofortunaamplexor@gmail.com", "@2GdH6L@cNHuHRAbyBPigHw");
                client.Send(message);
                message = null;
                this.Close();

            }

            catch (Exception)
            {
                MessageBox.Show("There is a problem occured while sending your feedback, please try again later");
            }

        }
    }
}
