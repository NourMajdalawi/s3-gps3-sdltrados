﻿using Sdl.Desktop.IntegrationApi;
using Sdl.Desktop.IntegrationApi.Extensions;
using Sdl.TranslationStudioAutomation.IntegrationApi;
using Sdl.TranslationStudioAutomation.IntegrationApi.Extensions;
using Sdl.TranslationStudioAutomation.IntegrationApi.Presentation.DefaultLocations;
using System;
using System.Collections.Generic;

namespace Feedback
{
    class Feedback
    {
		[RibbonGroup("Sdl.feedback", Name = "")]
		[RibbonGroupLayout(LocationByType = typeof(TranslationStudioDefaultRibbonTabs.HomeRibbonTabLocation))]
		class GCSButton : AbstractRibbonGroup
		{
		}

		[Action("Sdl.feedback", Name = "GCS - Feedback", Icon = "feedbackIcon", Description = "GCS - Feedback")]
		[ActionLayout(typeof(GCSButton), 20, DisplayType.Large)]
		class GCSButtonViewPartAction : AbstractAction
		{
			protected override void Execute()
			{
				frmFeedback frmFeedback = new frmFeedback();
				frmFeedback.ShowDialog();
			}

		}
	}
}
