﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GCS_TranslationMemory;
using System.IO;
using System.Collections.Generic;

namespace GCS_TranslationMemory_Test
{
    [TestClass]
    public class EUWebServiceTest
    {
        [TestMethod]
        public void CallWebService()
        {
            string query = "DTA = 2020 AND DTN = 1432";
            string celex = EUWebService.returnCelexNumber(query, "en");
            Assert.AreEqual("32020R1432", celex);

        }
        [TestMethod]
        [ExpectedException(typeof(Exception))] 
        public void CallWebServiceWithException()
        {
            string query = "DTA = 2020 AND DTN = 1432";
            string searchURL = "http://eur-lex.europa.eu/searche";
            EUWebService.CallWebService(query, "en", true, searchURL);

        }
    }
}
