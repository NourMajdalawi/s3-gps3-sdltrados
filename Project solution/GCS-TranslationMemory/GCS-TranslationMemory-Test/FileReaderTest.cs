﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;

namespace GCS_TranslationMemory_Test
{
    [TestClass]
    public class FileReaderTest
    {
        [TestMethod]
        public void getReferenceNumebrsTest()
        {
            HashSet<String> matches = new HashSet<string>();

            matches.Add("a");
            matches.Add("b");
            matches.Add("b");
            matches.Add("c");

            List<string> list = matches.ToList<string>();

            Assert.AreEqual(list.Count, 3);
        }
    }
}
