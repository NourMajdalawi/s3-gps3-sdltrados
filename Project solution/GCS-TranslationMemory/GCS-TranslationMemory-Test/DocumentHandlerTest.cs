﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GCS_TranslationMemory;
using System.IO;
using System;
using GCS_TranslationMemory.Log_Files;

namespace GCS_TranslationMemory_Test
{
    [TestClass]
    public class DocumentHandlerTest
    {
        private static string loggersTestPath = @"C:\GCS-TranslationMemoryFiles\Loggers_Test\";
        private static string htmlPath = @"C:\GCS-TranslationMemoryFiles\HTML-Documents\";
        private string loggerTestFileName;

        [TestInitialize]
        public void InitializeTest()
        {
            if (!Directory.Exists(htmlPath) || !Directory.Exists(loggersTestPath))
            {
                Directory.CreateDirectory(htmlPath);
                Directory.CreateDirectory(loggersTestPath);
            }
            this.loggerTestFileName = $"{loggersTestPath}logger_test.txt";
        }
        [TestMethod]
        public void DownloadDocumentTest()
        {
            DocumentHandler.downloadHTMLDocuments("2020", "1432", "en", "nl");
            string fileName_s = "CELEX_32020R1432_en.html";
            string fileName_t = "CELEX_32020R1432_nl.html";
            Assert.IsTrue(File.Exists(htmlPath + fileName_s));
            Assert.IsTrue(File.Exists(htmlPath + fileName_t));
            DocumentHandler.deleteHTMLFiles();
        }
        [TestMethod]
        public void GenerateTMX()
        {
            // generate an empty tmx file. (we don't download any HTML file to add tmx content)
            DocumentHandler.generateTmxFile("nl", "en", loggerTestFileName, "NL-NL");
            DocumentHandler.Save();

            string TMXpath = $"CELEX__nl_en.tmx";
            Assert.IsTrue(File.Exists(htmlPath + TMXpath));
        }
        [TestMethod]
        public void AlignTmx()
        {
            string reference = "2020/1432";
            DocumentHandler.alignTMX(reference, "nl", "en", "NL-NL", loggerTestFileName);
            DocumentHandler.Save();
            string fileName_s = "CELEX_32020R1432_nl_en.tmx";
            Assert.IsTrue(File.Exists(htmlPath + fileName_s));
        } 
    }
}

