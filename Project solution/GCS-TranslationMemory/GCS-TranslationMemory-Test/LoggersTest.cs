﻿using GCS_TranslationMemory;
using GCS_TranslationMemory.Log_Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GCS_TranslationMemory_Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class LoggersTest
    {
        private static string loggersTestPath = @"C:\GCS-TranslationMemoryFiles\Loggers_Test\";
        private string loggerTestFileName;

        [TestInitialize]
        public void InitializeTest()
        {
            if (!Directory.Exists(loggersTestPath))
            {
                Directory.CreateDirectory(loggersTestPath);
            }
            this.loggerTestFileName = $"{loggersTestPath}logger_test.txt";
        }
        [TestMethod]
        public void retrieveTheErrors()
        {
            Assert.AreEqual(0, Logger.ReadFile(loggerTestFileName).Count);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void AlignTmxTestExceptionNoLongerInForce()
        {
            /* This reference number is no longer in force,
             * check if the method through an exception*/
            string referenceNumber = "1977/2412";
            DocumentHandler.alignTMX(referenceNumber, "en", "nl", "EN-EN", loggerTestFileName);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GenerateTmxTestExceptionReferenceDoesNotExist()
        {
            /* This reference number is not exist in the EUR-Lex,
             * check if the method through an exception*/
            string referenceNumber = "999999/999999";
            DocumentHandler.alignTMX(referenceNumber, "en", "nl", "EN-EN", loggerTestFileName);
        }
    }
}
