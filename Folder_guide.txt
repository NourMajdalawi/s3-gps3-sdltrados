Documentation folder:
 - Extras -> all other supporting documents like TICT reports, Cultural dimentions comparison, current business proccess etc. 
 - MoM -> minutes of the first few meetings
 - Presentations -> all sprint planning, review and retrospective presentations
 - Sample SDLXLIFF files -> Example files used in the solution proccess
 - Testing -> contains all Test Plans and Reports;
Project Plan.pdf
Research Report.pdf

Project Solution folder:
 - GCS-TranslationMemory -> Main solution 
 - Sprint1-FirstVersion -> Hello World plugin made for the first sprint

Deployed version:
GCS-TranslationMemory.sdlplugin -> Deployed plugin*

*if the SDL Trados version is not matching use  -> HtmlAgilityPack.dll
add this file to {installed directory}/SDL{version}/
 